from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium import webdriver
from decimal import Decimal
import pandas as pd
import time
import re

class Twitter():
    def __init__(self, email, password, url_profile, browser='Chrome'):
        LOGIN_URL = 'https://twitter.com/login'

        self.email = email
        self.password = password
        self.url_profile = url_profile
        self.post = None

        self.like_count_post = 0
        self.text_post = " "
        self.date_post = " "

        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        chrome_options.add_experimental_option("prefs", prefs)

        if browser == 'Chrome':
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=chrome_options)
        elif browser == 'Firefox':
            self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        self.driver.get(LOGIN_URL)
        time.sleep(1)

    def Login(self):
        email_element = self.driver.find_element_by_name('session[username_or_email]')
        email_element.send_keys(self.email)

        password_element = self.driver.find_element_by_name('session[password]')
        password_element.send_keys(self.password)

        login_button = self.driver.find_element_by_xpath("//div[@class='css-901oao r-1awozwy r-jwli3a r-6koalj r-18u37iz r-16y2uox r-1qd0xha r-a023e6 r-vw2c0b r-1777fci r-eljoum r-dnmrzs r-bcqeeo r-q4m81j r-qvutc0']")
        login_button.click()

        time.sleep(2)

    def Scraping(self):
        self.driver.get(self.url_profile)
        time.sleep(2)

        try:
            profile_info = WebDriverWait(self.driver, 100).until(
                EC.presence_of_element_located((By.XPATH, "//div[@class='css-1dbjc4n r-16y2uox']")))

            followers_count = profile_info.find_element_by_xpath(".//div[@class='css-1dbjc4n r-18u37iz r-1w6e6rj']/div[2]")

            followers_count = self.clean(followers_count.text.split()[0])
            self.follower_count = self.format(followers_count)

            posts_links = []

            for i in range(7):
                all = self.driver.find_element_by_xpath("//div[@class='css-1dbjc4n r-1jgb5lz r-1ye8kvj r-13qz1uu']/div[2]")
                all = all.find_element_by_xpath(".//section[@class='css-1dbjc4n']")
                posts = all.find_elements_by_xpath(".//div[@class='css-1dbjc4n']/div/div")

                self.driver.execute_script("window.scrollBy(0, 500);")
                time.sleep(7)

                for post in posts:
                    try:
                        self.post = post
                        if self.check_exists_by_xpath(".//div[@class='css-1dbjc4n r-1d09ksm r-18u37iz r-1wbh5a2']"):
                            link = post.find_element_by_xpath(".//div[@class='css-1dbjc4n r-1d09ksm r-18u37iz r-1wbh5a2']/a")
                            posts_links.append(link.get_attribute('href'))
                    except:
                        continue

            posts_links = set(posts_links)

            for link in posts_links:
                self.driver.get(link)
                time.sleep(3)

                self.like_count = 0
                self.comment_count = 0

                post = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@class='css-1dbjc4n r-my5ep6 r-qklmqi r-1adg3ll']")))

                self.post = post
                if self.check_exists_by_xpath(".//div[@class='css-1dbjc4n r-1gkumvb r-1efd50x r-5kkj8d r-18u37iz r-tzz3ar r-ou255f r-9qu9m4']"):
                    like_comment = post.find_element_by_xpath(".//div[@class='css-1dbjc4n r-1gkumvb r-1efd50x r-5kkj8d r-18u37iz r-tzz3ar r-ou255f r-9qu9m4']")
                    self.post = like_comment

                    if self.check_exists_by_xpath(".//div[1]"):
                        if re.search('Like',like_comment.find_element_by_xpath(".//div[1]").text):
                            like_count = like_comment.find_element_by_xpath(".//div[1]").text.split("\n")[0]
                            like_count = self.clean(like_count)
                            like_count = Decimal(self.format(like_count))
                            if like_count >= self.like_count_post:
                                self.like_count_post = like_count

                                date = post.find_element_by_xpath(".//div[@class='css-1dbjc4n r-vpgt9t']")
                                date = date.find_element_by_xpath(".//span[@class='css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0']")
                                self.date_post = date.text.split(' · ')[1]

                        if self.check_exists_by_xpath(".//div[2]"):
                            like_count = like_comment.find_element_by_xpath(".//div[2]").text.split("\n")[0]
                            like_count = self.clean(like_count)
                            like_count = Decimal(self.format(like_count))
                            if like_count >= self.like_count_post:
                                self.like_count_post = like_count

                                date = post.find_element_by_xpath(".//div[@class='css-1dbjc4n r-vpgt9t']")
                                date = date.find_element_by_xpath(".//span[@class='css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0']")
                                self.date_post = date.text.split(' · ')[1]

            print("like_count :", self.like_count_post)
            print("date of post :",self.date_post)
        except:
            print("Loading took too much time!")

    def format(self ,word):
        if re.search("K", word):
            return Decimal(word.replace('K', ''))*1000
        elif re.search("M", word):
            return Decimal(word.replace('M', ''))*1000000
        else:
            return word

    def clean(self, string):
        if re.search(",",string):
            string = string.replace(",","")
        if re.search("View all",string):
            string = string.replace("View all","")
        if re.search("comments",string):
            string = string.replace("comments","")
        if re.search("views",string):
            string = string.replace("views","")
        return string

    def check_exists_by_xpath(self, xpath):
        try:
            self.post.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True