from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium import webdriver
from decimal import Decimal
import pandas as pd
import time
import re

class LinkedIn():
    def __init__(self, email, password, url_profile, browser='Chrome'):
        LOGIN_URL = 'https://www.linkedin.com/login'

        self.email = email
        self.password = password
        self.url_profile = url_profile
        self.post = None

        self.like_count_post = 0
        self.text_post = " "
        self.date_post = " "

        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        chrome_options.add_experimental_option("prefs", prefs)

        if browser == 'Chrome':
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=chrome_options)
        elif browser == 'Firefox':
            self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        self.driver.get(LOGIN_URL)
        time.sleep(1)

    def Login(self):
        email_element = self.driver.find_element_by_id('username')
        email_element.send_keys(self.email)

        password_element = self.driver.find_element_by_id('password')
        password_element.send_keys(self.password)

        login_button = self.driver.find_element_by_xpath("//div[@class='login__form_action_container ']")
        login_button.click()

        time.sleep(2)

    def Scraping(self):
        self.driver.get(self.url_profile)
        time.sleep(2)

        # scroll
        self.driver.execute_script("window.scrollBy(0, 900);")
        time.sleep(7)

        profile_info = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@class='profile-detail']")))
        activity = profile_info.find_element_by_xpath(".//div[5]")

        see_all = activity.find_element_by_xpath(".//span[@class='pv-profile-section__section-info']")
        self.driver.implicitly_wait(10)
        ActionChains(self.driver).move_to_element(see_all).click(see_all).perform()

        # scroll
        for i in range(7):
            self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(5)

        try:
            posts = self.driver.find_element_by_xpath("//div[@id='voyager-feed']")

            for post in posts.find_elements_by_xpath(".//div[@class='occludable-update ember-view']"):

                self.post = post

                if self.check_exists_by_xpath(".//span[@class='v-align-middle social-details-social-counts__reactions-count']"):
                    likes_count = post.find_element_by_xpath(".//span[@class='v-align-middle social-details-social-counts__reactions-count']")
                    like_count = Decimal(self.clean(likes_count.text))

                    if like_count >= self.like_count_post:
                        self.like_count_post = like_count

                        date = post.find_element_by_xpath(".//span[@class='feed-shared-actor__sub-description t-12 t-black--light t-normal']")
                        self.date_post = date.text.split('\n')[1]

                        if self.check_exists_by_xpath(".//span[@class='break-words']"):
                            self.text_post = post.find_element_by_xpath(".//span[@class='break-words']").text

            print("text post :",self.text_post)
            print("likes_count ", self.like_count_post)
            print("date of post :",self.date_post)

        except:
            print("Loading took too much time!")

    def clean(self, string):
        if re.search(",",string):
            string = string.replace(",","")
        if re.search("Comments",string):
            string = string.replace("Comments","")
        if re.search("Views",string):
            string = string.replace("Views","")
        return string

    def check_exists_by_xpath(self, xpath):
        try:
            self.post.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True







