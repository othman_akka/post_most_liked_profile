from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium import webdriver
from decimal import Decimal
import pandas as pd
import time
import re

class Facebook():
    def __init__(self, email, password, url_profile, browser='Chrome'):
        LOGIN_URL = 'https://www.facebook.com/login.php'

        self.email = email
        self.password = password
        self.url_profile = url_profile
        self.post = None

        self.like_count_post = 0
        self.text_post = " "
        self.date_post = " "

        self.chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        self.chrome_options.add_experimental_option("prefs", prefs)

        if browser == 'Chrome':
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=self.chrome_options)
        elif browser == 'Firefox':
            self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        self.driver.get(LOGIN_URL)
        time.sleep(1)

    def Login(self):
        email_element = self.driver.find_element_by_id('email')
        email_element.send_keys(self.email)

        password_element = self.driver.find_element_by_id('pass')
        password_element.send_keys(self.password)

        login_button = self.driver.find_element_by_id('loginbutton')
        login_button.click()

        time.sleep(2)

    def Scraping(self):
        self.driver.get(self.url_profile)

        self.driver.get(self.url_profile+"posts/")

        # scroll
        for i in range(1, 7):
            self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(5)

        try:
            posts = WebDriverWait(self.driver,100).until(EC.presence_of_element_located((By.XPATH,"//div[@class='_2pie _14i5 _1qkq _1qkx']")))
            posts = posts.find_elements_by_xpath(".//div[@class='_4-u2 _4-u8']")

            for post in posts:

                self.post = post

                if self.check_exists_by_xpath(".//div[@class='_66lg']"):
                    like_count = post.find_element_by_xpath(".//div[@class='_66lg']")
                    like_count = like_count.find_element_by_xpath(".//a[@class='_3dlf']")
                    # replace M by 000000 and K by 000
                    like_count = self.format(like_count.text.split()[0])

                    if Decimal(like_count) >= self.like_count_post:
                        date = post.find_element_by_xpath(".//div[@class='_6a _5u5j _6b']")
                        date = date.find_element_by_xpath(".//span[@class='l_wk1uwor3g']")
                        self.date_post = date.text
                        if self.check_exists_by_xpath(".//div[@class='_5pbx userContent _3576']"):
                            self.text_post = post.find_element_by_xpath(".//div[@class='_5pbx userContent _3576']").text
                        else :
                            self.text_post = " "

                        self.like_count_post = Decimal(like_count)

                    #self.likes_count.append(self.like_count)
            print("date of post :", self.date_post)
            print("text of post :", self.text_post)
            print("like_count :", self.like_count_post)
        except:
            print("Loading took too much time!")

    def format(self ,word):
        if re.search("K", word):
            return Decimal(word.replace('K', ''))*1000
        elif re.search("M", word):
            return Decimal(word.replace('M', ''))*1000000
        else:
            return word

    def check_exists_by_xpath(self, xpath):
        try:
            self.post.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True