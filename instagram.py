from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium import webdriver
from decimal import Decimal
import pandas as pd
import time
import re

class Instagram():
    def __init__(self, email, password, url_profile, browser='Chrome'):
        LOGIN_URL = 'https://www.instagram.com/accounts/login/'

        self.email = email
        self.password = password
        self.url_profile = url_profile
        self.post = None

        self.like_count_post = 0
        self.text_post = " "
        self.date_post = " "

        self.chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        self.chrome_options.add_experimental_option("prefs", prefs)
        mobile_emulation = {"deviceName": "Nexus 5"}
        self.chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)

        if browser == 'Chrome':
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=self.chrome_options)
        elif browser == 'Firefox':
            self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        self.driver.get(LOGIN_URL)
        time.sleep(1)

    def Login(self):
        button_facebook = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//button[@class='sqdOP  L3NKy   y3zKF     ']")))
        self.driver.implicitly_wait(10)
        ActionChains(self.driver).move_to_element(button_facebook).click(button_facebook).perform()

        email_element = self.driver.find_element_by_name('email')
        email_element.send_keys(self.email)

        password_element = self.driver.find_element_by_name('pass')
        password_element.send_keys(self.password)

        login_button = self.driver.find_element_by_name('login')
        login_button.click()

        time.sleep(5)
        if self.check_exists_by_xpath("//button[contains(text(),'Cancel')]"):
            self.driver.find_element_by_xpath("//button[contains(text(),'Cancel')]").click()

    def Scraping(self):
        self.driver.get(self.url_profile)
        if self.check_exists_by_xpath("//div[@class='v9tJq  VfzDr']"):
            followers_count = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@class='v9tJq  VfzDr']/ul/li[2]")))
            self.follower_count = self.format(followers_count.text.split()[0])

            list_links = []

            try:
                for i in range(4):
                    self.driver.implicitly_wait(10)
                    posts = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@class='v9tJq  VfzDr']")))
                    posts = posts.find_element_by_xpath(".//div[@class=' _2z6nI']/article/div[1]/div")
                    cells = posts.find_elements_by_xpath(".//div[@class='Nnq7C weEfm']")

                    for cell in cells:
                        posts = cell.find_elements_by_xpath(".//div[@class='v1Nh3 kIKUG  _bz0w']")
                        for post in posts:
                            link = post.find_element_by_xpath(".//a").get_attribute('href')
                            list_links.append(link)

                    self.driver.execute_script("window.scrollBy(0, 500);")
                    time.sleep(4)

                list_links = set(list_links)

                for link in list_links:

                    self.driver.get(link)
                    time.sleep(5)

                    if self.check_exists_by_xpath("//div[@class='HbPOm _9Ytll']"):
                        likes_count = self.driver.find_element_by_xpath("//div[@class='HbPOm _9Ytll']")
                        like_count = Decimal(self.clean(likes_count.text))
                        if like_count >= self.like_count_post:
                            self.like_count_post = like_count
                            self.date = self.driver.find_element_by_xpath("//div[@class='k_Q0X NnvRN']/a").text
                            print("likes1 :", self.like_count_post)

                    elif self.check_exists_by_xpath("//a[@class='zV_Nj']"):
                        likes_count = self.driver.find_element_by_xpath("//a[@class='zV_Nj']/span")
                        like_count = Decimal(self.clean(likes_count.text))
                        if like_count >= self.like_count_post:
                            self.like_count_post = like_count
                            self.date = self.driver.find_element_by_xpath("//div[@class='k_Q0X NnvRN']/a").text
                            print("likes2 :", self.like_count_post)

            except:
                print("Loading took too much time!")

    def format(self ,word):
        if re.search("k", word):
            return Decimal(word.replace('k', ''))*1000
        elif re.search("m", word):
            return Decimal(word.replace('m', ''))*1000000
        else:
            return word

    def clean(self, string):
        if re.search(",",string):
            string = string.replace(",","")
        if re.search("View all",string):
            string = string.replace("View all","")
        if re.search("comments",string):
            string = string.replace("comments","")
        if re.search("views",string):
            string = string.replace("views","")
        return string

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True
